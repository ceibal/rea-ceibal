<?php namespace Ceibal\Rearatings\Components;

use Cms\Classes\ComponentBase;
use Cms\Classes\Page;
use Response;

class Voto extends ComponentBase
{
    public $faqs;

    public function componentDetails()
    {
        return [
            'name'        => 'Voto',
            'description' => 'Componente para votar.'
        ];
    }

    public function defineProperties()
    {
        return [];
    }



    public function onRun()
    {

    }
}
