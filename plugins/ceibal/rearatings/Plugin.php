<?php namespace Ceibal\Rearatings;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Ceibal\Rearatings\Components\Voto'             => 'voto'
        ];

    }

    public function registerSettings()
    {
    }
}
