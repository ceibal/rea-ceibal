<?php
    use Ceibal\ReaRatings\Models\Voto;

    Route::get('backend/ceibal/vote/{id}/{val}', function($id,$val)
    {

        if(isset($_COOKIE['voto']))
        {
            $arr = array($_COOKIE['voto']);
           // Caduca en un año
           if(!in_array($id,$arr))
           {
               setcookie('voto', $_COOKIE['voto'] + "," + $id, time() + 365 * 24 * 60 * 60);
               votar($id, $val);
           }else {
               # Tengo que modificar el voto
               $votoAnterior    = Voto::where("id_recurso",$id)
                                ->where("ip_user",getUserIpAddr())->first();
               if($votoAnterior)
               {
                    $idVoto          = $votoAnterior->id;
                    votar($id, $val, $idVoto);
               }
           }

        }
        else
        {
           // Caduca en un año
          setcookie('voto', $id, time() + 365 * 24 * 60 * 60);
          votar($id, $val);
        }



        return Response::json("ok");
    });

    function votar($id,$val,$idVoto = NULL)
    {
        $ipUser = getUserIpAddr();
        //$votoAnterior = Voto::where("id",$idVoto)->update(["ip_user"=>getUserIpAddr(),"id_recurso"=>$id]);
        if($idVoto)
        {
            $voto               = Voto::where("id",$idVoto)->update(["voto"=>$val]);
        }
        else
        {
            $votoAnterior = Voto::where("id_recurso",$id)
                            ->where("ip_user",$ipUser)->first();
            if($votoAnterior){
                $voto               = Voto::where("id",$votoAnterior->id)->update(["voto"=>$val]);
            }else {
                $voto               = new Voto;
                $voto->id_recurso   = $id;
                $voto->voto         = $val;
                $voto->ip_user      = $ipUser;
                $voto->save();
            }

        }

    }

    function getUserIpAddr()
    {
        if(!empty($_SERVER['HTTP_CLIENT_IP'])){
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

?>
