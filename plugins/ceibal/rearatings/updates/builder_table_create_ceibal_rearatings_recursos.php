<?php namespace Ceibal\Rearatings\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateCeibalRearatingsRecursos extends Migration
{
    public function up()
    {
        Schema::create('ceibal_rearatings_recursos', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('id_recurso');
            $table->integer('voto');
            $table->string('ip_user', 50);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ceibal_rearatings_recursos');
    }
}
