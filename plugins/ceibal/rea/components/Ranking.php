<?php namespace Ceibal\Rea\Components;

use Ceibal\Rea\Models\Categoria;
use Ceibal\Rea\Models\Tag;
use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use Ceibal\Rea\Models\Recurso as RecursoRea;
use Illuminate\Support\Facades\DB;
use System\Models\File;

class Ranking extends ComponentBase
{
    /** @var  Variable para listar los Recursos */
    public $posts;

    public $resourcePage;
    public $maxItems;
    public $filtro;
    public $pageParam;
    public $currentPage;
    public $totalPages;

    public function componentDetails()
    {
        return [
            'name'        => 'Ranking de recursos',
            'description' => 'Componente para mostrar el ranking'
        ];
    }

    public function defineProperties()
    {
        return [
            'resourcePage'  => [
                'title'             => 'Página del recurso',
                'description'       => 'Define cuál va a ser la URL asociada a los elementos listados',
                'type'              => 'dropdown',
                'default'           => 'rea/Recurso'
            ],
            'filtro'        => [
                'title'             => 'Filtrar por',
                'description'       => 'Define por qué tipo de elemento se desea filtrar',
                'type'              => 'dropdown',
                'default'           => 'none',
                'options'           => ['none' => 'Sin filtro', 'category' => 'Categorías', 'tag' => 'Tags']
            ],
            'maxItems'      => [
                'title'             => 'Cantidad máxima',
                'description'       => 'La cantidad máxima de elementos en la página',
                'default'           => 10,
                'type'              => 'string',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'El valor ingresado debe ser numérico'
            ],
            'pageNumber'    => [
                'title'             => 'Número de la página',
                'description'       => 'Utilizado para indicar cuál es la página actual',
                'type'              => 'string',
                'default'           => '{{ :page }}',
            ]
        ];
    }

    public function getResourcePageOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function onRun()
    {
        $this->pageParam    = $this->page['pageParam'] = $this->paramName('pageNumber');
        $this->resourcePage = $this->page['resourcePage'] = $this->property('resourcePage');
        $this->filtro       = $this->page['filtro'] = $this->property('filtro');
        $this->maxItems     = $this->page['maxItems'] = $this->property('maxItems');

        if(isset($_GET['page']) && !empty($_GET['page']))
        {
            $currentPage         = $_GET['page'];
        }else
        {
            $currentPage         = 1;
        }

        $totalP = DB::select('SELECT count(distinct id_recurso) as count FROM ceibal_rearatings_recursos
                            JOIN ceibal_rea_recursos on ceibal_rea_recursos.id=ceibal_rearatings_recursos.id_recurso 
                            WHERE publicado=1');

        if($totalP[0]->count > $this->maxItems)
        {
                $offset         = ($currentPage - 1) * $this->maxItems;
                $totalPages     = ceil($totalP[0]->count / $this->maxItems);

        }
        else
        {
            $offset         = 0;
            $totalPages     = 1;
        }

        $this->posts = $this->page['posts'] = DB::select('SELECT AVG(voto) as promedio,ceibal_rea_recursos.*,ceibal_rea_categorias.color as color, 
                                                        ceibal_rea_categorias.nombre as nombre FROM ceibal_rearatings_recursos 
                                                        JOIN ceibal_rea_recursos on ceibal_rea_recursos.id=ceibal_rearatings_recursos.id_recurso 
                                                        JOIN ceibal_rea_categorias ON ceibal_rea_recursos.categoria_id=ceibal_rea_categorias.id 
                                                        WHERE publicado=1 GROUP BY id_recurso ORDER BY promedio DESC LIMIT '.$this->maxItems.' OFFSET '.$offset);

        $this->page['totalPages']   = $totalPages;
        $this->page['currentPage']  = $currentPage;

        foreach ($this->posts as $recurso) {
            $recurso->descripcion = strip_tags($recurso->descripcion);
            $fileNameImage = DB::select('SELECT disk_name FROM system_files WHERE attachment_type LIKE "%Recurso" AND field LIKE "imagen" AND attachment_id='.$recurso->id);

            $folder1    = substr($fileNameImage[0]->disk_name, 0,3);
            $folder2    = substr($fileNameImage[0]->disk_name, 3,3);
            $folder3    = substr($fileNameImage[0]->disk_name, 6,3);

            $file       = File::where('disk_name',$fileNameImage[0]->disk_name)->first();

            $recurso->imagen = $file;
            $promedio = DB::select('SELECT AVG(voto) as promedio FROM ceibal_rearatings_recursos WHERE id_recurso='.$recurso->id);

            $recurso->stars = intval($promedio[0]->promedio);
        }
    }
}
