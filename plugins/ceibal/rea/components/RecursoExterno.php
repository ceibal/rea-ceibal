<?php namespace Ceibal\Rea\Components;

use Cms\Classes\ComponentBase;

class RecursoExterno extends ComponentBase
{

    public $recursoexterno;

    public function componentDetails()
    {
        return [
            'name'        => 'Recurso Externo',
            'description' => 'Visualización de la ficha del Recurso Externo'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getResourcePageOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function onRun()
    {

        $recurso_slug = $this->param('slug');

        $this->recursoexterno = $this->page['recursoexterno'] = \Ceibal\Rea\Models\RecursosExternos::where('slug','=',$recurso_slug)->first();

    }
}
