<?php namespace Ceibal\Rea\Components;

use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use Ceibal\Rea\Models\Coleccion as ColeccionRea;
use Ceibal\Rea\Models\Recurso as RecursoRea;
use Ceibal\Rea\Models\RecursosExternos as RecursoExterno;
use Response, BackendAuth;

class Coleccion extends ComponentBase
{

    public $coleccion;
    public $posts;
    public $resourcePage;
    public $resourcePageExternos;
    public $maxItems;
    public $pageParam;
    public $recursosexternos;

    public function componentDetails()
    {
        return [
            'name'        => 'Colección',
            'description' => 'Componente para mostrar una colección de recursos'
        ];
    }

    public function defineProperties()
    {
        return [
            'resourcePage'  => [
                'title'             => 'Página del recurso',
                'description'       => 'Define cuál va a ser la URL asociada a los elementos listados',
                'type'              => 'dropdown',
                'default'           => 'rea/Recurso'
            ],
            'maxItems'      => [
                'title'             => 'Cantidad máxima',
                'description'       => 'La cantidad máxima de elementos en la página',
                'default'           => 10,
                'type'              => 'string',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'El valor ingresado debe ser numérico'
            ],
            'pageNumber'    => [
                'title'             => 'Número de la página',
                'description'       => 'Utilizado para indicar cuál es la página actual',
                'type'              => 'string',
                'default'           => '{{ :page }}',
            ]
        ];
    }

    public function getResourcePageOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function onRun()
    {
        $coleccion_slug = $this->param('slug');

        $this->pageParam    = $this->page['pageParam'] = $this->paramName('pageNumber');
        $this->resourcePage = $this->page['resourcePage'] = $this->property('resourcePage');
        $this->maxItems     = $this->page['maxItems'] = $this->property('maxItems');

        $this->coleccion    = $this->page['coleccion'] = ColeccionRea::where('slug','=',$coleccion_slug)->first();

        $this->posts        = $this->page['posts'] = $this->coleccion->recursos()->orderBy('created_at', 'desc')->paginate(8);

        foreach ($this->posts as $recurso) {
            $recurso->descripcion = strip_tags($recurso->descripcion);
        }
        $this->recursosexternos = $this->page['recursosexternos'] = $this->coleccion->recursosexternos()->orderBy('created_at', 'desc')->paginate(8);

        foreach ($this->recursosexternos as $recursoexterno){
            $recursoexterno->description = strip_tags($recursoexterno->description);
        }

        $this->posts = $this->posts->merge($this->recursosexternos);

    }
}
