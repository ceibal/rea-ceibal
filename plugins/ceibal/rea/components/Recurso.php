<?php namespace Ceibal\Rea\Components;

use Cms\Classes\ComponentBase;
use Ceibal\Rea\Models\Recurso as RecursoRea;
use Ceibal\Rearatings\Models\Voto;
use Response, BackendAuth;

class Recurso extends ComponentBase
{
    /** @var  Recurso REA */
    public $post;
    public $rating;

    public function componentDetails()
    {
        return [
            'name'        => 'Ficha recurso',
            'description' => 'Componente para mostrar la ficha de un recurso'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        // $recurso_id = $this->param('recurso_id');
        $recurso_slug = $this->param('slug');
        // PARA UTILIZAR LA FORMA ESTRICTA Y RELAJADA
        // SE PUEDE ACCEDER A LA VARIABLE:
        // {{ componente.post }} (estricto)
        // {{ post }} (relajada)

        $this->showTags = $this->page['showTags'] = false;
        $this->showRelated = $this->page['showRelated'] = false;

        $this->post = $this->page['post'] = RecursoRea::where('slug','=',$recurso_slug)->first();//RecursoRea::findOrFail($recurso_slug);

        if(count($this->post->relacionados()->get()) > 0 )
        {
            $this->showRelated = $this->page['showRelated'] = true;
        }


        if(count($this->post->tags) > 0)
        {
          $this->showTags = $this->page['showTags'] = true;
        }


        $votos = \DB::select('SELECT AVG(voto) as rating FROM ceibal_rearatings_recursos
WHERE id_recurso='.$this->post->id);
        $this->rating =  $this->page['rating'] = intval($votos[0]->rating);

        if (!$this->post->publicado && !(BackendAuth::check()))
        {
            return \Redirect::to('404');
        }

    }
}
