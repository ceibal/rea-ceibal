<?php
/**
 * Created by PhpStorm.
 * User: pablo
 * Date: 10/2/17
 * Time: 11:10 AM
 */

namespace Ceibal\Rea\Controllers;
use BackendMenu;
use Backend\Classes\Controller;
use Ceibal\Rea\Models\Categoria;
use Lang;

class Categorias extends Controller {

    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public $requiredPermissions = ['ceibal.rea.access_categorias', 'ceibal.rea.reorder_categorias'];

    public function __construct() {
        parent::__construct();
        BackendMenu::setContext('Ceibal.Rea', 'rea', 'categorias');
    }

    public function reorder() {
        BackendMenu::setContext('Ceibal.Rea', 'rea', 'reorder');
        $this->pageTitle = 'Reordenar Categorías';
        $toolbarConfig = $this->makeConfig();
        $toolbarConfig->buttons = 'plugins/ceibal/rea/controllers/categorias/_reorder_toolbar.htm';
        $this->vars['toolbar'] = $this->makeWidget('Backend\Widgets\Toolbar', $toolbarConfig);
        $this->vars['records'] = Categoria::make()->getEagerRoot();
    }

    public function reorder_onMove() {
        $sourceNode = Categoria::find(post('sourceNode'));
        $targetNode = post('targetNode') ? Categoria::find(post('targetNode')) : null;
        if($sourceNode == $targetNode) {
            return;
        }
        switch (post('position')) {
            case 'before':
                $sourceNode->moveBefore($targetNode);
                break;
            case 'after':
                $sourceNode->moveAfter($targetNode);
                break;
            case 'child':
                $sourceNode->makeChildOf($targetNode);
                break;
            default:
                $sourceNode->makeRoot();
                break;
        }
    }

    public function generateCSV(){

        $url = "https://rea.ceibal.edu.uy";
        $fields = ['Titulo', 'Nivel', 'Autor', 'Url', 'Descripción', 'Usuario Creador','Categoría'];

        //we create the CSV into memory
        $csv = Writer::createFromFileObject(new \SplTempFileObject());

        //we insert the CSV header
        $csv->insertOne($fields);

        $recursos = Recurso::select('ceibal_rea_recursos.titulo', 'ceibal_rea_recursos.nivel', 'ceibal_rea_recursos.autor_name',
            'ceibal_rea_recursos.url_visualizar', 'ceibal_rea_recursos.descripcion', 'backend_users.first_name' , 'backend_users.last_name',
            'ceibal_rea_categorias.nombre as categoría')
            ->join('ceibal_rea_categorias', 'ceibal_rea_categorias.id', '=', 'ceibal_rea_recursos.categoria_id')
            ->join('backend_users', 'backend_users.id', '=', 'ceibal_rea_recursos.user_id')
            ->get();

        foreach ($recursos as $recurso => $value){
            $csv->insertOne([$value->titulo,
                $value->nivel,
                $value->autor_name,
                $url.$value->url_visualizar,
                str_replace("&nbsp;", " ", strip_tags($value->descripcion)),
                $value->first_name." ".$value->last_name,
                $value->categoría]);
        }
        $csv->output('recursos.csv');
        die;
    }

}

?>