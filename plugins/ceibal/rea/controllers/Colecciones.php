<?php namespace Ceibal\Rea\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Colecciones extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public $requiredPermissions = ['ceibal.rea.access_colecciones'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Ceibal.Rea', 'rea', 'colecciones');

    }
}
