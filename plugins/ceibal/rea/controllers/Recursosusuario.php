<?php
/**
 * Created by PhpStorm.
 * User: pablo
 * Date: 10/2/17
 * Time: 11:10 AM
 */

namespace Ceibal\Rea\Controllers;
use Backend\Models\User;use BackendMenu;
use Backend\Classes\Controller;
use Ceibal\Rea\Models\Categoria;
use Ceibal\Rea\Controllers\Recursos;
use Ceibal\Rea\Models\Recurso;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use Lang;
use Event;
use BackendAuth;
use League\Csv\Writer;

class RecursosUsuario extends Recursos {


    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.ImportExportController'
    ];

     public $formConfig = 'config_form.yaml';
     public $listConfig = 'config_list.yaml';

    public $requiredPermissions = ['ceibal.rea.access'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Ceibal.Rea', 'rea', 'misrecursos');

        Event::listen('backend.list.extendColumns', function($widget)
        {
            if ($widget->getController() instanceof Recursos )
            {
                $widget->bindEvent('list.extendQuery', function($query)
                {
                    if(isset($_GET['e'])&& !empty($_GET['e']))
                    {

                        switch ($_GET['e'])
                        {
                            case 5:
                            $query->where('publicado',0)
                                        ->where('etapaUno','=',1)
                                        ->where('etapaDos','=',1)
                                        ->where('etapaTres','=',1)
                                        ->where('etapaCuatro','=',1)
                                        ->where('etapaCinco','=',0)->get();
                            break;
                            case 4:
                                $query->where('etapaUno','=',1)
                                        ->where('etapaDos','=',1)
                                        ->where('etapaTres','=',1)
                                        ->where('etapaCuatro','=',0)
                                        ->where('publicado',0)->get();
                                break;
                            case 3:
                            $query->where('publicado',0)
                                  ->where('etapaUno','=',1)
                                  ->where('etapaDos','=',1)
                                  ->where('etapaTres','=',0)->get();
                            break;

                            case 2:

                                $query->where('publicado',0)
                                      ->where('etapaUno','=',1)
                                      ->where('etapaDos','=',0)->get();
                                break;

                            case 1:
                            $query->where('publicado',0)
                                  ->where('etapaUno','=',0)->get();
                            break;

                            default:
                            //$query = Recurso::all();
                            break;
                        }
                    }
                });
            }
        });
    }

    public function onUserFilter()
    {
        return [];
    }

    // Concatena los datos personales del usuarios
    public function boot()
    {
        UserModel::extend(function($model) {
             $model->addDynamicMethod('getFullNameAttribute', function ($value) use ($model) {
                 if($model) {
                     return $model->name . ' ' . $model->surname;
                 } else { return ''; }
            });
        });
    }

    public function onExport(){

        $url = "https://rea.ceibal.edu.uy";

       $usuarios = Input::get('usuarios');
       $etapas = Input::get('etapas');
       $niveles = Input::get('niveles');
       $categorias = Input::get('categorias');

        $fields = ['Titulo', 'Nivel', 'Autor', 'Url', 'Descripción', 'Usuario Creador','Categoría'];

        //we create the CSV into memory
        $csv = Writer::createFromFileObject(new \SplTempFileObject());

        //we insert the CSV header
        $csv->insertOne($fields);


        if (empty($usuarios) && empty($etapas) && empty($niveles) && empty($categorias))
       {

           $recursos = Recurso::select('ceibal_rea_recursos.titulo', 'ceibal_rea_recursos.nivel', 'ceibal_rea_recursos.autor_name',
               'ceibal_rea_recursos.url_visualizar', 'ceibal_rea_recursos.descripcion', 'backend_users.first_name' , 'backend_users.last_name',
               'ceibal_rea_categorias.nombre as categoría')
               ->join('ceibal_rea_categorias', 'ceibal_rea_categorias.id', '=', 'ceibal_rea_recursos.categoria_id')
               ->join('backend_users', 'backend_users.id', '=', 'ceibal_rea_recursos.user_id')
               ->get();

           foreach ($recursos as $recurso => $value){
               $csv->insertOne([$value->titulo,
                   $value->nivel,
                   $value->autor_name,
                   $url.$value->url_visualizar,
                   str_replace("&nbsp;", " ", strip_tags($value->descripcion)),
                   $value->first_name." ".$value->last_name,
                   $value->categoría]);
           }
           $csv->output('recursos.csv');
           die;

       }else{
            if ($etapas){
                if ($etapas == 1){
                    $recursos = Recurso::select('ceibal_rea_recursos.titulo', 'ceibal_rea_recursos.nivel', 'ceibal_rea_recursos.autor_name',
                        'ceibal_rea_recursos.url_visualizar', 'ceibal_rea_recursos.descripcion', 'backend_users.first_name' , 'backend_users.last_name',
                        'ceibal_rea_categorias.nombre as categoría')
                        ->join('ceibal_rea_categorias', 'ceibal_rea_categorias.id', '=', 'ceibal_rea_recursos.categoria_id')
                        ->join('backend_users', 'backend_users.id', '=', 'ceibal_rea_recursos.user_id')
                        ->where('etapaUno','=',0)
                        ->orWhere('user_id', $usuarios)
                        ->orWhere('nivel', $niveles)
                        ->orwhere('categoria_id', $categorias)
                        ->get();

                    foreach ($recursos as $recurso => $value){
                        $csv->insertOne([$value->titulo,
                            $value->nivel,
                            $value->autor_name,
                            $url.$value->url_visualizar,
                            str_replace("&nbsp;", " ", strip_tags($value->descripcion)),
                            $value->first_name." ".$value->last_name,
                            $value->categoría]);
                    }

                    $csv->output('recursos.csv');
                    die;
                }
                if ($etapas == 2) {
                    $recursos = Recurso::select('ceibal_rea_recursos.titulo', 'ceibal_rea_recursos.nivel', 'ceibal_rea_recursos.autor_name',
                        'ceibal_rea_recursos.url_visualizar', 'ceibal_rea_recursos.descripcion', 'backend_users.first_name', 'backend_users.last_name',
                        'ceibal_rea_categorias.nombre as categoría')
                        ->join('ceibal_rea_categorias', 'ceibal_rea_categorias.id', '=', 'ceibal_rea_recursos.categoria_id')
                        ->join('backend_users', 'backend_users.id', '=', 'ceibal_rea_recursos.user_id')
                        ->where('etapaUno', '=', 1)
                        ->where('etapaDos', '=', 0)
                        ->orWhere('user_id', $usuarios)
                        ->orWhere('nivel', $niveles)
                        ->orwhere('categoria_id', $categorias)
                        ->get();

                    foreach ($recursos as $recurso => $value) {
                        $csv->insertOne([$value->titulo,
                            $value->nivel,
                            $value->autor_name,
                            $url.$value->url_visualizar,
                            str_replace("&nbsp;", " ", strip_tags($value->descripcion)),
                            $value->first_name . " " . $value->last_name,
                            $value->categoría]);
                    }

                    $csv->output('recursos.csv');
                    die;
                }
                if ($etapas == 3) {
                    $recursos = Recurso::select('ceibal_rea_recursos.titulo', 'ceibal_rea_recursos.nivel', 'ceibal_rea_recursos.autor_name',
                        'ceibal_rea_recursos.url_visualizar', 'ceibal_rea_recursos.descripcion', 'backend_users.first_name', 'backend_users.last_name',
                        'ceibal_rea_categorias.nombre as categoría')
                        ->join('ceibal_rea_categorias', 'ceibal_rea_categorias.id', '=', 'ceibal_rea_recursos.categoria_id')
                        ->join('backend_users', 'backend_users.id', '=', 'ceibal_rea_recursos.user_id')
                        ->where('etapaUno', '=', 1)
                        ->where('etapaDos', '=', 1)
                        ->where('etapaTres', '=', 0)
                        ->orWhere('user_id', $usuarios)
                        ->orWhere('nivel', $niveles)
                        ->orwhere('categoria_id', $categorias)
                        ->get();

                    foreach ($recursos as $recurso => $value) {
                        $csv->insertOne([$value->titulo,
                            $value->nivel,
                            $value->autor_name,
                            $url.$value->url_visualizar,
                            str_replace("&nbsp;", " ", strip_tags($value->descripcion)),
                            $value->first_name . " " . $value->last_name,
                            $value->categoría]);
                    }

                    $csv->output('recursos.csv');
                    die;
                }
                if ($etapas == 4) {
                    $recursos = Recurso::select('ceibal_rea_recursos.titulo', 'ceibal_rea_recursos.nivel', 'ceibal_rea_recursos.autor_name',
                        'ceibal_rea_recursos.url_visualizar', 'ceibal_rea_recursos.descripcion', 'backend_users.first_name', 'backend_users.last_name',
                        'ceibal_rea_categorias.nombre as categoría')
                        ->join('ceibal_rea_categorias', 'ceibal_rea_categorias.id', '=', 'ceibal_rea_recursos.categoria_id')
                        ->join('backend_users', 'backend_users.id', '=', 'ceibal_rea_recursos.user_id')
                        ->where('etapaUno', '=', 1)
                        ->where('etapaDos', '=', 1)
                        ->where('etapaTres', '=', 1)
                        ->where('etapaCuatro', '=', 0)
                        ->orWhere('user_id', $usuarios)
                        ->orWhere('nivel', $niveles)
                        ->orwhere('categoria_id', $categorias)
                        ->get();

                    foreach ($recursos as $recurso => $value) {
                        $csv->insertOne([$value->titulo,
                            $value->nivel,
                            $value->autor_name,
                            $url.$value->url_visualizar,
                            str_replace("&nbsp;", " ", strip_tags($value->descripcion)),
                            $value->first_name . " " . $value->last_name,
                            $value->categoría]);
                    }

                    $csv->output('recursos.csv');
                    die;
                }
                else{
                    $recursos = Recurso::select('ceibal_rea_recursos.titulo', 'ceibal_rea_recursos.nivel', 'ceibal_rea_recursos.autor_name',
                        'ceibal_rea_recursos.url_visualizar', 'ceibal_rea_recursos.descripcion', 'backend_users.first_name', 'backend_users.last_name',
                        'ceibal_rea_categorias.nombre as categoría')
                        ->join('ceibal_rea_categorias', 'ceibal_rea_categorias.id', '=', 'ceibal_rea_recursos.categoria_id')
                        ->join('backend_users', 'backend_users.id', '=', 'ceibal_rea_recursos.user_id')
                        ->where('etapaUno', '=', 1)
                        ->where('etapaDos', '=', 1)
                        ->where('etapaTres', '=', 1)
                        ->where('etapaCuatro', '=', 1)
                        ->where('etapaCinco', '=', 0)
                        ->orWhere('user_id', $usuarios)
                        ->orWhere('nivel', $niveles)
                        ->orwhere('categoria_id', $categorias)
                        ->get();

                    foreach ($recursos as $recurso => $value) {
                        $csv->insertOne([$value->titulo,
                            $value->nivel,
                            $value->autor_name,
                            $url.$value->url_visualizar,
                            str_replace("&nbsp;", " ", strip_tags($value->descripcion)),
                            $value->first_name . " " . $value->last_name,
                            $value->categoría]);
                    }

                    $csv->output('recursos.csv');
                    die;
                }
            }else{
               $recursos = Recurso::select('ceibal_rea_recursos.titulo', 'ceibal_rea_recursos.nivel', 'ceibal_rea_recursos.autor_name',
                   'ceibal_rea_recursos.url_visualizar', 'ceibal_rea_recursos.descripcion', 'backend_users.first_name' , 'backend_users.last_name',
                   'ceibal_rea_categorias.nombre as categoría')
                   ->join('ceibal_rea_categorias', 'ceibal_rea_categorias.id', '=', 'ceibal_rea_recursos.categoria_id')
                   ->join('backend_users', 'backend_users.id', '=', 'ceibal_rea_recursos.user_id')
                   ->where('user_id', $usuarios)
                   ->orWhere('nivel', $niveles)
                   ->orwhere('categoria_id', $categorias)
                   ->get();

               foreach ($recursos as $recurso => $value){
                   $csv->insertOne([$value->titulo,
                       $value->nivel,
                       $value->autor_name,
                       $url.$value->url_visualizar,
                       str_replace("&nbsp;", " ", strip_tags($value->descripcion)),
                       $value->first_name." ".$value->last_name,
                       $value->categoría]);
               }

               $csv->output('recursos.csv');
               die;
            }
       }

    }


}

?>
