<?php namespace Ceibal\Rea\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Ceibal\Rea\Models\Recurso;
use League\Csv\Writer;
use Illuminate\Support\Facades\Input;

class Recursos extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.ImportExportController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $importExportConfig = 'config_import_export.yaml';


    public $requiredPermissions = ['ceibal.rea.access'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Ceibal.Rea', 'rea', 'recursos');

    }

    public function generateCSV(){

        $url = "https://rea.ceibal.edu.uy";
        $fields = ['Titulo', 'Nivel', 'Autor', 'Url', 'Descripción', 'Usuario Creador','Categoría'];

        //we create the CSV into memory
        $csv = Writer::createFromFileObject(new \SplTempFileObject());

        //we insert the CSV header
        $csv->insertOne($fields);

        $recursos = Recurso::select('ceibal_rea_recursos.titulo', 'ceibal_rea_recursos.nivel', 'ceibal_rea_recursos.autor_name',
            'ceibal_rea_recursos.url_visualizar', 'ceibal_rea_recursos.descripcion', 'backend_users.first_name' , 'backend_users.last_name',
            'ceibal_rea_categorias.nombre as categoría')
            ->join('ceibal_rea_categorias', 'ceibal_rea_categorias.id', '=', 'ceibal_rea_recursos.categoria_id')
            ->join('backend_users', 'backend_users.id', '=', 'ceibal_rea_recursos.user_id')
            ->get();

        foreach ($recursos as $recurso => $value){
            $csv->insertOne([$value->titulo,
                $value->nivel,
                $value->autor_name,
                $url.$value->url_visualizar,
                str_replace("&nbsp;", " ", strip_tags($value->descripcion)),
                $value->first_name." ".$value->last_name,
                $value->categoría]);
        }
        $csv->output('recursos.csv');
        die;
    }

}
