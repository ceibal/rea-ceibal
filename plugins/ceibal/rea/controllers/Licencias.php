<?php namespace Ceibal\Rea\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Licencias Back-end Controller
 */
class Licencias extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Ceibal.Rea', 'rea', 'licencias');
    }
}
