<?php namespace Ceibal\Rea\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Recursos Externo Back-end Controller
 */
class RecursosExterno extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public $requiredPermissions = ['ceibal.rea.access'];


    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Ceibal.Rea', 'rea', 'recursosexternos');
    }
}
