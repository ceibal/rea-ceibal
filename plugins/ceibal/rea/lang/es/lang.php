<?php

return [
    'plugin' => [
        'name'          => 'REA',
        'description'   => 'Plugin de prueba para poder cargar y visualizar archivos ExeLearning'
    ],
    'rea' => [
        'create_resource'   => 'Crear recurso',
        'resource_created'  => 'Recurso creado',
        'update_resource'   => 'Actualizar recurso',
        'resource_updated'  => 'Recurso actualizado',
        'resource_deleted'  => 'Recurso borrado',
        'preview_resource'  => 'Previsualizar recurso',
        'create_category'   => 'Crear categoría',
        'create_collection'   => 'Crear colección',
        'category_created'  => 'Categoría creada',
        'update_category'   => 'Actualizar categoría',
        'category_updated'  => 'Categoría actualizada',
        'category_deleted'  => 'Categoría borrada',
        'create_resource_extern'   => 'Crear recurso externo',
        'update_resource_extern'   => 'Actualizar recurso externo',


        // LISTA Y FORMULARIO
        'resource_image'        => 'Imagen del recurso',
        'imagen_coleccion'      => 'Imagen de la colección',
        'id'                    => 'Identificación',
        'title'                 => 'Título',
        'published'             => 'Publicado',
        'date'                  => 'Fecha creación',
        'description'           => 'Descripción',
        'objectives'            => 'Propositos',
        'zip'                   => 'Recurso Exelearning - zip',
        'elp'                   => 'Archivo elp',
        'level'                 => 'Nivel educativo',
        'category_name'         => 'Nombre',
        'category_description'  => 'Descripción',
        'category_color'        => 'Color',
        'category_hidden'       => 'Oculto',
        'license_name'          => 'Nombre de la licencia CC',
        'license_type'          => 'Tipo de licencia CC',
        'license_url'           => 'URL a licencia CC',
        'license_image'         => 'Imagen de licencia CC',
        'conocimientos_previos' => 'Conocimientos previos',
        'slug'                  => 'Slug',
        'edu_primaria'          => 'Educación Inicial y Primaria',
        'edu_secundaria'        => 'Educación Media y Superior',
        'edu_ambos'             => 'Ambos niveles',
        'categoria'             => 'Categoría',
        'etiquetas'             => 'Etiquetas',
        'licencia'              => 'Licencia CC',
        'principal'             => 'Principal recurso en home',
        'destacado'             => 'Destacado',
        'autor'                 => 'Autor',
        'formato'               => 'Formato',
        'recomendaciones'       => 'Recomendaciones',
        'published_on'          => 'Fecha de publicación',
        'export_published'      => 'Exportar recursos publicados',
        'url_visualizar'        => 'URL',
        'autor_name'            => 'Nombre de autor',
        'origin'                => 'Origen',
        'date_consulted'        => 'Fecha de consulta'


    ]
];
