<?php
namespace Ceibal\Rea\Models;

use Backend\Models\ExportModel;

class EntriesExport extends ExportModel {
    public $table = 'ceibal_rea_recursos';

    public function exportData($columns, $sessionKey = null)
    {
        $entries =  Recurso::where('publicado', 1)->get();
        $entries->each(function($subscriber) use ($columns) {
            $subscriber->addVisible($columns);
        });
        return $entries->toArray();
    }
}