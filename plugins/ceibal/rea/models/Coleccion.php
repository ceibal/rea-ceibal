<?php namespace Ceibal\Rea\Models;

use Model,BackendAuth;

/**
 * Model
 */
class Coleccion extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\SoftDelete;

    use \October\Rain\Database\Traits\Sluggable;

    protected $dates = ['deleted_at'];

    /*
     * Validation
     */
    public $rules = [
        'titulo'            => 'required|max:150',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ceibal_rea_coleccion';

    /**
     * @var array Generate slugs for these attributes.
     */
    protected $slugs = ['slug' => 'titulo'];

    public $belongsToMany = [
        'recursos' => ['Ceibal\Rea\Models\Recurso', 'table' =>'ceibal_rea_coleccion_recurso', 'timestamps' => true,],
        'recursosexternos' => ['Ceibal\Rea\Models\RecursosExternos', 'table' =>'ceibal_rea_coleccion_recursoexterno', 'timestamps' => true,]
    ];

    // Relación recursos externos
   /* public $belongsTo = [
        'recursosexternos' => 'Ceibal\Rea\Models\RecursosExternos'
    ];*/

    public $attachOne = [
        'imagen' => 'System\Models\File'
    ];


}
