<?php namespace Ceibal\Rea\Models;

use Model;
use ZipArchive;
use BackendAuth;
use Illuminate\Support\Facades\DB;

/**
 * Model
 */
class RecursoRating extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ceibal_rearatings_recursos';

    // PARA OBTENER RECURSOS RELACIONADOS POR TAGS
    public function scopeRanking($query)
    {
        return $query;
    }

}
