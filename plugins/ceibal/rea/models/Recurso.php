<?php namespace Ceibal\Rea\Models;

use Model;
use ZipArchive;
use BackendAuth;
use Illuminate\Support\Facades\DB;

/**
 * Model
 */
class Recurso extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\SoftDelete;

    use \October\Rain\Database\Traits\Sluggable;

    protected $dates = ['deleted_at'];

    /*
     * Validation
     */
    public $rules = [
        'titulo'            => 'required|max:150',
        'filezip'           => 'required',
        'fileelp'           => 'required',
        'nivel'             => 'required|numeric|min:0|max:3',
        'propositos'        => 'required',
        'descripcion'       => 'required',
        'imagen'            => 'required',
        'autor_name'        => 'required'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ceibal_rea_recursos';

    /**
     * @var array Generate slugs for these attributes.
     */
    protected $slugs = ['slug' => 'titulo'];

    // RELACIÓN CON CATEGORÍAS Y TAGS
    public $belongsToMany = [
        'tags'       => ['Ceibal\Rea\Models\Tag','table' => 'ceibal_rea_tag_recurso','timestamps' => true],
        'coleccion'  => ['Ceibal\Rea\Models\Coleccion','table' => 'ceibal_rea_coleccion_recurso','timestamps' => true,'conditions' => 'published = 1']
    ];

    // RELACIÓN CON LICENCIA CC
    public $belongsTo = [
        'categoria' => 'Ceibal\Rea\Models\Categoria',
        'licencia'  => 'Ceibal\Rea\Models\Licencia',
        'autor'     => ['Backend\Models\User','key'=>'user_id']
    ];

    public $attachOne = [
        'imagen' => 'System\Models\File',
        'filezip' => 'System\Models\File',
        'fileelp' => 'System\Models\File'
    ];



    private function delete_files($target)
    {
        if(is_dir($target)){
            $files = glob( $target . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned

            foreach( $files as $file )
            {
                $this->delete_files( $file );
            }

            @rmdir( $target );
        } elseif(is_file($target)) {
            unlink( $target );
        }
    }

    private function zipManager()
    {
        $zip          = new ZipArchive();
        $path         = base_path();
        $folder       = '/storage/app/media';
        $folderPath   = $folder.'/recursos/';

        if (empty($this->slug))
        {
            unset($this->slug);
            $this->setSluggedValue('slug', 'titulo');
        }
        $nameFolder   = $this->slug;
        if(!is_null($nameFolder) && isset($nameFolder) && !empty($nameFolder) && (strlen($nameFolder) > 2))
        {
            if (is_dir($path.$folderPath.$this->slug))
            {
                $this->delete_files($path.$folderPath.$this->slug);
            }

            $res = $zip->open($this->filezip()->withDeferred($this->sessionKey)->latest()->first()->getLocalPath());

            $zip->extractTo($path.$folderPath.$this->slug);

            $this->url_visualizar = '/elp/' . $nameFolder . '/inicio';
        }

        $analytics = '<!-- Global site tag (gtag.js) - Google Analytics --> 
                        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-9349693-43"></script> 
                        <script> window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag(\'js\', new Date());gtag(\'config\', \'UA-9349693-43\');
                        </script>
                      </head>';

        $directorio = $path.$folderPath.$this->slug;
        if (is_dir($directorio)){
            if ($dh = opendir($directorio)){
                while (($files = readdir($dh)) !== false){
                    $ext=substr($files,-4);
                        if ($ext == 'html'){
                            $html = file_get_contents($directorio."/".$files);
                            $htmls = str_replace('</head>', $analytics, $html);
                            #echo  $html;
                            $fp =fopen($directorio."/".$files,'w');
                            fwrite($fp, $htmls);
                            fclose($fp);
                        }
                }
                closedir($dh);
            }
        }


      /*$zip = new ZipArchive();
      $path = base_path();
      $folder = '/storage/app/media';
      $folderPath = $folder.'/recursos/';

      var_dump($this->slug);
      if (is_dir($path.$folderPath.$this->slug))
      {
        $this->delete_files($path.$folderPath.$this->slug);
      }

      $res = $zip->open($this->filezip()->withDeferred($this->sessionKey)->latest()->first()->getLocalPath());

      $zip->extractTo($path.$folderPath.$this->slug);

      $this->url_visualizar = '/elp/' . $this->slug . '/inicio';*/

    }
    // Para descomprimir el zip cuando se genera un nuevo recurso
    // Hay que ver cuando se actualiza un recurso y cuando coinciden los nombres de los archivos zip
    public function beforeCreate()
    {

      $user = BackendAuth::getUser();
      $this->autor = $user->id;

      if($this->principal)
      {
          $recurso = Recurso::where('principal','=',1)->update(['principal' => 0]);
      }
    }

    public function afterCreate()
    {
        $user = BackendAuth::getUser();
        $this->autor = $user->id;
        $roleId = $user->attributes['role_id'];

        $rol = DB::table('backend_user_roles')->where('id',$roleId)->first();

        $recurso = Recurso::where('id',$this->id)->update(['etapaUno' => 0,'etapaDos'=>0,'etapaTres' => 0,'etapaCuatro' => 0, 'etapaCinco' => 0]);

    }

    public function beforeUpdate()
    {
      if($this->principal && ($this->getOriginal('principal') != $this->principal))
      {
          $recurso = Recurso::where('principal','=',1)->update(['principal' => 0]);
      }
    }

    public function beforeSave()
    {
        $this->zipManager();
    }

    public function afterSave()
    {
        if($this->publicado)
        {
            $recurso = Recurso::where('id',$this->id)->update(['published_on' => now()]);
        }

        if($this->getOriginal('destacado') != $this->destacado)
        {
            if($this->destacado)
            {
                $hoy = date("Y-m-d H:i:s");
                DB::table('ceibal_rea_destacado')->insert([
                    ['recurso_id' => $this->id, 'created_at' => $hoy, 'updated_at' => $hoy]
                ]);
            }else
            {
                DB::table('ceibal_rea_destacado')->where('recurso_id',$this->id)->delete();
            }
        }
    }




    // QUERY PARA OBTENER RECURSOS PUBLICADOS
    public function scopePublicados($query)
    {
        return $query->where('publicado', 1)
                ->where('deleted_at',null);
    }

    // PARA OBTENER TODOS LOS RECURSOS SIN EL ACTUAL
    public function otros()
    {
        return Recurso::where('id','<>',$this->id);
    }

    public function scopeNivel($query,$level)
    {
      return $query->where('nivel',$level)
      ->orWhere('nivel',3);
    }

    // PARA OBTENER RECURSOS RELACIONADOS POR TAGS
    public function relacionados(){
        return $this->otros()->whereHas('tags', function ($query) {
            $query->whereIn('slug',$this->tags->lists('slug'));
        });
    }
}
