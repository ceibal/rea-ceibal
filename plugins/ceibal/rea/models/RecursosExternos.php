<?php namespace Ceibal\Rea\Models;

use Model;

/**
 * Model
 */
class RecursosExternos extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var array Validation rules
     */

    public $rules = [
        'title'            => 'required|unique:ceibal_rea_recursos_externos|max:150',
        'slug'             => 'unique:ceibal_rea_recursos_externos|max:150',
        'origin'           => 'required|max:150',
        'date_consulted'   => 'required',
        'autor'            => 'required',
        'description'      => 'required',
    ];

    /**
     * @var string The database table used by the model.
     */

    public $table = 'ceibal_rea_recursos_externos';

    public $attachOne = [
        'imagen' => 'System\Models\File'
    ];


}
