<?php namespace Ceibal\Rea\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateRecursoDestacado extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('ceibal_rea_destacado'))
        {
            Schema::create('ceibal_rea_destacado', function($table)
            {
                $table->engine = 'InnoDB';
                $table->increments('destacado_id');
                $table->integer('recurso_id');
                $table->timestamps();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('ceibal_rea_destacado');
    }
}
