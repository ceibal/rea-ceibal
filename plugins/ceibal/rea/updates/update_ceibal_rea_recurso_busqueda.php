<?php namespace Ceibal\Rea\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateCeibalReaRecursoBusqueda extends Migration
{
    public function up()
    {
        if (!Schema::hasColumn('ceibal_rea_recursos','buscador'))
        {
            Schema::table('ceibal_rea_recursos', function($table)
            {
                $table->text('buscador')->nullable();
            });
        }
    }

    public function down()
    {
        Schema::table('ceibal_rea_recursos', function ($table) {
            $table->dropColumn('buscador');
        });
    }
}
