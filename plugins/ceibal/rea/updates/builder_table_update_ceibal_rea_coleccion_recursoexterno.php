<?php namespace Ceibal\Rea\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateCeibalReaColeccionRecursoexterno extends Migration
{
    public function up()
    {
        Schema::table('ceibal_rea_coleccion_recursoexterno', function($table)
        {
            $table->increments('id');
            $table->integer('recursos_externos_id');
            $table->integer('coleccion_id');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('ceibal_rea_coleccion_recursoexterno', function($table)
        {
            $table->integer('id')->change();
        });
    }
}
