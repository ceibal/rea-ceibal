<?php namespace Ceibal\Rea\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateCeibalReaColeccionRecurso extends Migration
{
    public function up()
    {
        Schema::table('ceibal_rea_coleccion_recurso', function($table)
        {
            $table->string('formato', 150);
        });
    }
    
    public function down()
    {
        Schema::table('ceibal_rea_coleccion_recurso', function($table)
        {
            $table->dropColumn('formato');
        });
    }
}
