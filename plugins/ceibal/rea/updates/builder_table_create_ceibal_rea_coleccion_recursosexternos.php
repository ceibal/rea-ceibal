<?php namespace Ceibal\Rea\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateCeibalReaColeccionRecursosexternos extends Migration
{
    public function up()
    {
        Schema::create('ceibal_rea_coleccion_recursoexterno', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('recursos_externos_id')->unsigned();
            $table->integer('coleccion_id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ceibal_rea_coleccion_recursosexternos');
    }
}
