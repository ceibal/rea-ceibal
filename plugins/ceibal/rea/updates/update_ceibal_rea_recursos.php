<?php namespace Ceibal\Rea\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateCeibalReaRecursos extends Migration
{
    public function up()
    {
        if (!Schema::hasColumn('ceibal_rea_recursos','categoria_id'))
        {
            Schema::table('ceibal_rea_recursos', function($table)
            {
                $table->integer('categoria_id')->unsigned()->nullable()->index()->after('licencia_id');
            });
        }
    }

    public function down()
    {
        Schema::table('ceibal_rea_recursos', function ($table) {
            $table->dropColumn('categoria_id');
        });
    }
}
