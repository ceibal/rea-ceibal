<?php namespace Ceibal\Rea\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateCeibalReaRecursosExternos extends Migration
{
    public function up()
    {
        Schema::create('ceibal_rea_recursos_externos', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('title', 150);
            $table->string('slug', 150);
            $table->text('description');
            $table->string('autor', 50);
            $table->string('origin', 150);
            $table->date('date_consulted');
            $table->string('formato', 150);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ceibal_rea_recursos_externos');
    }
}
