<?php namespace Ceibal\Rea\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateCeibalReaRecursosRecomendaciones extends Migration
{
    public function up()
    {
        Schema::table('ceibal_rea_recursos', function($table)
        {
            $table->text('recomendaciones')->nullable()->after('autor_name');
        });
    }

    public function down()
    {

        $table->dropColumn('recomendaciones');
    }
}
