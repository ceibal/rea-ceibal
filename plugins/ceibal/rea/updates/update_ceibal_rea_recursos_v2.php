<?php namespace Ceibal\Rea\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateCeibalReaRecursosV2 extends Migration
{
    public function up()
    {
        if (!Schema::hasColumn('ceibal_rea_recursos','user_id'))
        {
            Schema::table('ceibal_rea_recursos', function($table)
            {
                $table->integer('user_id')->unsigned()->nullable()->after('slug');
                $table->integer('visitas')->unsigned()->nullable()->after('nivel');
                $table->string('conocimientos_previos',100)->nullable()->after('visitas');
                $table->integer('autor_name')->unsigned()->nullable()->after('conocimientos_previos');
                $table->text('formato')->nullable()->after('conocimientos_previos');


            });
        }
    }

    public function down()
    {
        Schema::table('ceibal_rea_recursos', function ($table) {
            $table->dropColumn('user_id');
            $table->dropColumn('visitas');
            $table->dropColumn('conocimientos_previos');
            $table->dropColumn('autor_name');
            $table->dropColumn('formato');
        });
    }
}
