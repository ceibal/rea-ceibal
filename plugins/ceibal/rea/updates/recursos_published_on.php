<?php namespace Ceibal\Rea\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class RecursosPublishedOn extends Migration
{
    public function up()
    {
        Schema::table('ceibal_rea_recursos', function($table)
        {
            $table->timestamp('published_on')->nullable()->after('publicado');
        });

    }

    public function down()
    {
        Schema::table('ceibal_rea_recursos', function ($table) {
            $table->dropColumn('published_on');
        });
    }
}
