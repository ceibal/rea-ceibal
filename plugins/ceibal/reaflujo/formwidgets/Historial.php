<?php

    namespace Ceibal\ReaFlujo\FormWidgets;

    use Backend\Classes\FormWidgetBase;
    use Config;

    class Historial extends FormWidgetBase {

        public function widgetDetails() {
            return [
                'name'        => 'Preview',
                'description' => 'Vista previa de los recursos.'
            ];
        }

        public function render()
        {
            $etUno      = $this->model->etapaUno;
            $etDos      = $this->model->etapaDos;
            $etTres     = $this->model->etapaTres;
            $etCuatro   = $this->model->etapaCuatro;
            $etCinco    = $this->model->etapaCinco;

            if(is_null($etCinco))
            {
                $etCinco = 0;
            }

            if(is_null($etDos))
            {
                $etDos = 0;
            }

            if(is_null($etTres))
            {
                $etTres = 0;
            }

            if(is_null($etCuatro))
            {
                $etCuatro = 0;
            }

            if(is_null($etCinco))
            {
                $etCinco = 0;
            }
            return $this->makePartial('historial',[
                'id'            => $this->model->id,
                'etapaUno'      => $etUno,
                'etapaDos'      => $etDos,
                'etapaTres'     => $etTres,
                'etapaCuatro'   => $etCuatro,
                'etapaCinco'    => $etCinco]);
        }

    }

?>
