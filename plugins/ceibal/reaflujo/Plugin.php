<?php namespace Ceibal\ReaFlujo;

use Backend;
use System\Classes\PluginBase;
use Ceibal\Rea\Updates\BackendUserSeeder;
use Ceibal\ReaFlujo\Classes\ManejadorRecursosUsuarios;

/**
 * rea_flujo Plugin Information File
 */
class Plugin extends PluginBase
{



    public $require  = ['Ceibal.Rea'];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'rea_flujo',
            'description' => 'Plugin de flujo para REA',
            'author'      => 'Exequiel Gonzalez',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        ManejadorRecursosUsuarios::init();
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Ceibal\ReaFlujo\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'ceibal.rea_flujo.some_permission' => [
                'tab' => 'rea_flujo',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'rea_flujo' => [
                'label'       => 'rea_flujo',
                'url'         => Backend::url('ceibal/rea_flujo/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['ceibal.rea_flujo.*'],
                'order'       => 500,
            ],
        ];
    }

    public function registerFormWidgets() {
        return [
            'Ceibal\ReaFlujo\FormWidgets\Historial' => [
                'code'  => 'historial',
                'label' => 'Historial'
            ]
        ];
    }
}
