<?php namespace Ceibal\ReaFlujo\Classes;

use Backend;
use BackendAuth;
use Event;

use Ceibal\ReaFlujo\Models\ReaPluginEtapaUserRoles;
use Ceibal\ReaFlujo\Models\ReaPluginLog;
use Ceibal\ReaFlujo\Models\ReaPluginEtapa;
use Ceibal\Rea\Controllers\Recursos;
use Ceibal\Rea\Models\Recurso;
use Backend\Models\User;
use Ceibal\ReaFlujo\Classes\MailSender;
use Illuminate\Support\Facades\DB;

/**
 * Esta clase se encarga de mostrar los recursos para los usuarios
 */
class ManejadorRecursosUsuarios
{

        public static function init()
        {
               self::prepararColumnasBackend();
               self::mostrarRecursos();
               self::prepararBotonesUsuarios();
               self::manejarFlujo();
        }

        private static function manejarFlujo()
        {


            Recurso::extend(function ($model){

                $model->bindEvent('model.afterSave', function() use ($model)
                {
                    $user           = BackendAuth::getUser();
                    $recursoId      = $model->id;
                    $userId         = $user->id;
                    $comentario     = $model->comentario;
                    $imagen         = $model->imagen;
                    # Completo la etapa y paso a la siguiente o las etapas :(

                    $etapaUnoId     = ReaPluginEtapa::obtenerEtapaId('etapaUno');
                    $etapaDosId     = ReaPluginEtapa::obtenerEtapaId('etapaDos');
                    $etapaTresId    = ReaPluginEtapa::obtenerEtapaId('etapaTres');
                    $etapaCuatroId  = ReaPluginEtapa::obtenerEtapaId('etapaCuatro');
                    $etapaCincoId   = ReaPluginEtapa::obtenerEtapaId('etapaCinco');

                    $template       = 'message';
                    $data           = [
                        'recurso'   =>$model->titulo,
                        'id'        =>$model->id,
                        'imagen'    =>$model->imagen
                    ];

                    if ($model->etapaCuatro == 1 && $model->publicado) {
                        $users = ReaPluginEtapa::obtenerUserPorEtapaCode('etapaCinco');
                        $data['estado'] = 'publicado en producción';
                        ReaPluginLog::insertarLogCompleto($etapaCincoId, $userId, $recursoId,$comentario,$users,$data);
                    }elseif ($model->etapaCuatro == 1) {
                        $users = ReaPluginEtapa::obtenerUserPorEtapaCode('etapaCinco')->where('role_id', 4);
                        $data['estado'] = 'para aprobar y publicar';
                        ReaPluginLog::insertarLogCompleto($etapaCuatroId, $userId, $recursoId,$comentario,$users,$data);
                    } elseif ($model->etapaTres == 1) {
                        $users = ReaPluginEtapa::obtenerUserPorEtapaCode('etapaCuatro')->where('role_id', 7);
                        $data['estado'] = 'para corregir diseño gráfico';
                        ReaPluginLog::insertarLogCompleto($etapaTresId, $userId, $recursoId,$comentario,$users,$data);
                    } elseif ($model->etapaDos == 1) {
                        $users = ReaPluginEtapa::obtenerUserPorEtapaCode('etapaTres')->where('role_id', 6);
                        $data['estado'] = 'para corregir estilo';
                        ReaPluginLog::insertarLogCompleto($etapaDosId, $userId, $recursoId,$comentario,$users,$data);
                    } elseif ($model->etapaUno == 1) {
                        # Obtengo los user ids de los potenciales siguiente etapa
                        $users = ReaPluginEtapa::obtenerUserPorEtapaCode('etapaDos')->where('role_id', 5);
                        $data['estado'] = 'para corregir diseño instruccional';
                        ReaPluginLog::insertarLogCompleto($etapaUnoId, $userId, $recursoId,$comentario,$users,$data);
                    }else {
                        $users = ReaPluginEtapa::obtenerUserPorEtapaCode('etapaUno')->where('role_id', 9);
                        $data['estado'] = 'creado';
                        ReaPluginLog::insertarLogCompleto($etapaUnoId, $userId, $recursoId,$comentario,$users,$data);
                    }
                });
            });
        }


        private static function prepararBotonesUsuarios()
        {
            Event::listen('backend.form.extendFields', function($widget){
               if (!$widget->getController() instanceof Recursos) {
                   return;
               }
               if (!$widget->model instanceof Recurso) {
                      return;
                  }

                  $historialButton = ['historial' => [
                          'label'=> "Historial",
                          'type'=>'Ceibal\ReaFlujo\FormWidgets\Historial',
                          'span'=>'right',
                  ]];
                  $widget->addFields($historialButton);


                  $comentario = ['comentario' => [
                          'label'=> "Comentario",
                          'type'=>'textarea',
                          'span'=>'left',
                  ]];
                  $widget->addFields($comentario);

                // Agregar los botones segun el tipo de etapa
                $user   = BackendAuth::getUser();
                $roleId = $user->attributes['role_id'];

                // Obtengo las etapas para este rol
                $etapasIds = ReaPluginEtapaUserRoles::select('etapaId')->where('roleId', $roleId)->get();
                $etapas = ReaPluginEtapa::select('nombre', 'code', 'accion')->wherein('id', $etapasIds)->get();
                $puedePublicar = 0;

                foreach ($etapas as $etapa) {
                    if ($etapa->code === 'etapaCinco') {
                        $puedePublicar = 1;
                    }
                }

		$rol = DB::table('backend_user_roles')->where('id',$roleId)->first();
		if($rol->code == 'administradorGlobal')
                {
                    $puedePublicar = 1;
                }

                /**
                 * Solo los que acceden a la etapa cinco puededen publicar
                 */
                if (!$puedePublicar) {
                   $widget->removeField('publicado');
                   $widget->removeField('destacado');
                   $widget->removeField('principal');
                }

                # Solo habilito la etapa en la que se trabaja
                $size = count($etapas);
                foreach ($etapas as $etapa) {
                    $working = $widget->model[$etapa->code];
                    if ($working===0) {
                        self::deshabilitarEtapas($widget, $etapas, $etapa->code);
                        break;
                    }
                }
        });
        }

        /**
         * Deshabilita todas las etapas salvo en la que se esta
         * trabajando si no es la ultima
         * @param  [type] $widget [description]
         * @param  [type] $code   [description]
         * @return [type]         [description]
         */
        private static function deshabilitarEtapas($widget, $etapas, $code)
        {
            $arrEtapas = ReaPluginEtapa::all();
            $codigoEtapa    = 0;
            switch ($code)
            {
                case 'etapaUno':
                        $codigoEtapa = 1;
                    break;
                case 'etapaDos':
                        $codigoEtapa = 2;
                    break;
                case 'etapaTres':
                        $codigoEtapa = 3;
                    break;
                case 'etapaCuatro':
                        $codigoEtapa = 4;
                    break;
                case 'etapaCinco':
                        $codigoEtapa = 5;
                    break;

                default:

                    break;
            }

            $user   = BackendAuth::getUser();
            $roleId = $user->attributes['role_id'];
            $rol    = DB::table('backend_user_roles')->where('id',$roleId)->first();

            $countEtapas = count($arrEtapas);
            for($i = 0; $i < $countEtapas; $i++)
            {
                if($i < 4)
                {
                    if($i < $codigoEtapa || $rol->code == 'administradorGlobal')
                    {
                        $label = $arrEtapas[$i]->accion;
                        if($i == ($codigoEtapa - 1) && $rol->code != 'administradorGlobal')
                        {
                            $label = "Apruebo mi etapa";
                        }
                        $newForm = [$arrEtapas[$i]->code => [
                                'label'=> $label,
                                'type'=>'switch',
                                'span'=>'full',
                        ]];
                        $widget->addFields($newForm);
                    }
                }
            }
        }

        /**
         * En este metodo se muestran los recursos segun el rol de el usuario
         * @param  [type] $widget [description]
         * @return [type]         [description]
         */
        private static function mostrarRecursos(){


            Event::listen('backend.list.extendColumns', function($widget)
            {
                if ($widget->getController() instanceof Recursos )
                {
                    $widget->bindEvent('list.extendQuery', function($query)
                    {

                        if($_SERVER['REQUEST_URI'] != '/backend/ceibal/rea/recursosusuario')
                        {
                            # Obtengo el usuario
                            ## Obtengo el role del usuario
                            $user   = BackendAuth::getUser();
                            $roleId = $user->attributes['role_id'];

                            ## Obtengo las etapas para este rol
                            $etapas = ReaPluginEtapaUserRoles::obtenerEtapasIdPorRol($roleId);

                            ## Obtengo los recursos que no estan asignados a nadie y los asignados al usuario

                            $recursosId = ReaPluginLog::obtenerRecursosXEtapaYUsuarioId($etapas, $user->id);

                            if(!isset($_GET['u']) && !isset($_GET['e']))
                            {
                                $query->whereIn('id', $recursosId);
                            }

                        }

                    });
                    # Agrego el estado para cada elemento segun la etapa
                    $widget->bindEvent('list.extendRecords', function($records){
                        $records->each(function($model){

                            $log = ReaPluginLog::select('etapaId', 'usuarioId')
                                                 ->where('recursoId', $model->id)->where('usuarioId','>',0)->orderBy('created_at', 'desc')->first();
                            if(isset($log->etapaId))
                            {
                                $etapaEstado    = ReaPluginEtapa::select('estado')->where('id', $log->etapaId)->first();
                                $usuarioLog     = User::select('login')->where('id', $log->usuarioId)->first();
                                $model->estado  = $model->publicado ? 'Publicado': $etapaEstado->estado;
                                $model->usuario = $usuarioLog ? $usuarioLog->login : 'n/a';
                            }
                        });
                    });
                 }
                });
        }

        /**
         * En este metodo se prepara el la vista del usuaria,
         * a la lista se le agrega las columnas estado.
         * @param  [type] $widget [description]
         * @return [type]         [description]
         */
        private static function prepararColumnasBackend()
        {
            Event::listen('backend.list.extendColumns', function($widget)
            {
                if ($widget->getController() instanceof Recursos )
                {
                    $widget->addColumns([
                        'estado'=>[
                            'label'=>'Estado',
                            'sortable' => false]
                    ]);
                    $widget->addColumns([
                        'usuario'=>[
                            'label'=>'Última modificación',
                            'sortable' => false]
                    ]);
                }

            });
        }

}
