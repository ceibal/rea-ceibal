<?php namespace Ceibal\ReaFlujo\Models;

use Ceibal\Rea\Models\Recurso;
use Model, Backend;

use Ceibal\ReaFlujo\Models\ReaPluginEtapaUserRoles;
use Ceibal\ReaFlujo\Models\ReaPluginEtapa;
use Ceibal\ReaFlujo\Classes\MailSender;
use Db;

class ReaPluginLog extends Model
{
    public $table = 'rea_plugin_log';

    public static function insertarLogCompleto($etapaId, $userId, $recursoId, $comentario = "", $users = NULL, $data = NULL)
    {
        $logOld = DB::select("SELECT * FROM rea_plugin_log
        WHERE recursoId = $recursoId
        ORDER BY rea_plugin_log.id DESC LIMIT 1");


        if (!$logOld){
            $log                = new ReaPluginLog;
            $log->etapaId       = $etapaId;
            $log->usuarioId     = $userId;
            $log->recursoId     = $recursoId;
            $log->comentario    = $comentario;
            $log->created_at    = date('Y-m-d H:i:s');
            $log->updated_at    = date('Y-m-d H:i:s');
            $log->save();

            MailSender::sendMany($users, $data, 'message');
        }else{
            $recursosEtapa = Recurso::where('id', $recursoId)->get();

            foreach ($recursosEtapa as $recurso){
                if ($recurso->etapaUno == 0 && ($recurso->etapaDos == 1 || $recurso->etapaTres == 1 || $recurso->etapaCuatro == 1)){
                    $log                = new ReaPluginLog;
                    $log->etapaId       = 1;
                    $log->usuarioId     = $userId;
                    $log->recursoId     = $recursoId;
                    $log->comentario    = $comentario;
                    $log->created_at    = date('Y-m-d H:i:s');
                    $log->updated_at    = date('Y-m-d H:i:s');
                    $log->save();

                    $users = ReaPluginEtapa::obtenerUserPorEtapaCode('etapaUno')->where('role_id', 9);
                    $data['estado'] = 'corregir recurso';
                    MailSender::sendMany($users, $data, 'message');

                }
                elseif ($recurso->etapaDos == 0 && ($recurso->etapaUno == 1 || $recurso->etapaTres == 1 || $recurso->etapaCuatro == 1)){
                    $log                = new ReaPluginLog;
                    $log->etapaId       = 2;
                    $log->usuarioId     = $userId;
                    $log->recursoId     = $recursoId;
                    $log->comentario    = $comentario;
                    $log->created_at    = date('Y-m-d H:i:s');
                    $log->updated_at    = date('Y-m-d H:i:s');
                    $log->save();

                    $users = ReaPluginEtapa::obtenerUserPorEtapaCode('etapaDos')->where('role_id', 5);
                    $data['estado'] = 'corregir diseño instruccional';
                    MailSender::sendMany($users, $data, 'message');
                }
                elseif ($recurso->etapaTres == 0 && ($recurso->etapaDos == 1 || $recurso->etapaDos == 1 || $recurso->etapaCuatro == 1)){
                    $log                = new ReaPluginLog;
                    $log->etapaId       = 3;
                    $log->usuarioId     = $userId;
                    $log->recursoId     = $recursoId;
                    $log->comentario    = $comentario;
                    $log->created_at    = date('Y-m-d H:i:s');
                    $log->updated_at    = date('Y-m-d H:i:s');
                    $log->save();

                    $users = ReaPluginEtapa::obtenerUserPorEtapaCode('etapaTres')->where('role_id', 6);
                    $data['estado'] = 'corregir estilo';
                    MailSender::sendMany($users, $data, 'message');
                }
                elseif ($recurso->etapaCuatro == 0 && ($recurso->etapaDos == 1 || $recurso->etapaTres == 1 || $recurso->etapaUno == 1)){
                    $log                = new ReaPluginLog;
                    $log->etapaId       = 4;
                    $log->usuarioId     = $userId;
                    $log->recursoId     = $recursoId;
                    $log->comentario    = $comentario;
                    $log->created_at    = date('Y-m-d H:i:s');
                    $log->updated_at    = date('Y-m-d H:i:s');
                    $log->save();

                    $users = ReaPluginEtapa::obtenerUserPorEtapaCode('etapaCuatro')->where('role_id', 7);
                    $data['estado'] = 'corregir diseño gráfico';
                    MailSender::sendMany($users, $data, 'message');
                }
                else{
                    $log                = new ReaPluginLog;
                    $log->etapaId       = 4;
                    $log->usuarioId     = $userId;
                    $log->recursoId     = $recursoId;
                    $log->comentario    = $comentario;
                    $log->created_at    = date('Y-m-d H:i:s');
                    $log->updated_at    = date('Y-m-d H:i:s');
                    $log->save();

                    MailSender::sendMany($users, $data, 'message');
                }
            }
        }
    }


    public static function obtenerRecursosXEtapaYUsuarioId($etapasId=[], $usuarioId=''){
            if ($etapasId)
            {
                $etapasAux = implode(',',$etapasId);
            }else{
                $etapasAux = '-1';
            }

            $et = 0;
            if(isset($etapasId[count($etapasId) - 1]))
            {
    			$et = $etapasId[count($etapasId) - 1];
    		}

            switch ($et)
            {
                case 5:
                    $recursosAux =  Db::select(Db::raw('SELECT id AS recursoId
                                                FROM ceibal_rea_recursos as rec
                                                where rec.etapaUno  = 1
                                                AND rec.etapaDos    = 1
                                                AND rec.etapaTres   = 1
                                                AND rec.etapaCuatro = 1
						AND (rec.etapaCinco = 0 OR rec.etapaCinco is NULL)
						AND rec.publicado = 0'));
                break;
                case 4:
                    $recursosAux =  Db::select(Db::raw('SELECT id AS recursoId
                                                FROM ceibal_rea_recursos as rec
                                                where rec.etapaUno  = 1
                                                AND rec.etapaDos    = 1
                                                AND rec.etapaTres   = 1
                                                AND rec.etapaCuatro = 0   
						AND rec.publicado = 0'));
                    break;
                case 3:
                    $recursosAux =  Db::select(Db::raw('SELECT id AS recursoId
                                                FROM ceibal_rea_recursos as rec
                                                where rec.etapaUno  = 1
                                                AND rec.etapaDos    = 1
                                                AND rec.etapaTres   = 0
                                                AND rec.publicado = 0'));
                    break;

                case 2:
                    $recursosAux =  Db::select(Db::raw('SELECT id AS recursoId
                                                FROM ceibal_rea_recursos as rec
                                                where rec.etapaUno  = 1
                                                AND rec.etapaDos    = 0
                                                AND rec.publicado = 0'));
                    break;

                case 1:
                    $recursosAux =  Db::select(Db::raw('SELECT id AS recursoId
                                                FROM ceibal_rea_recursos as rec
                                                where rec.etapaUno  = 0
                                                AND rec.publicado = 0'));
                    break;

                    default:
                    $recursosAux = [];
                    break;
                }

    	$recursosId = [];
    	foreach ($recursosAux as $recursoAux)
        {
    		array_push($recursosId, $recursoAux->recursoId);
    	}
    	return $recursosId;
    }

    public static function obtenerLogsXRecursoId($recursoId)
    {
    	$count = ReaPluginLog::where('recursoId', $recursoId)->count();
    	if ($count) {
 			return ReaPluginLog::where('recursoId', $recursoId)->get();
    	}

    	return 0;
    }

}
