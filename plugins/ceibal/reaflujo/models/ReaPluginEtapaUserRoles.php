<?php namespace Ceibal\ReaFlujo\Models;

use Model;

class ReaPluginEtapaUserRoles extends Model{
    public $table = 'rea_plugin_etapa_user_roles';


    public static function obtenerEtapasIdPorRol($roleId){

    	$queryEtapas = ReaPluginEtapaUserRoles::select('etapaId')->where('roleId', $roleId)->get();

    	$etapaIds = [];
    	foreach ($queryEtapas as $queryEtapa) {
    		# code...
    		array_push($etapaIds, $queryEtapa->etapaId);
    	}	
    	return $etapaIds;
    }

        public static function obtenerEtapasPorRol($roleId){

        return ReaPluginEtapaUserRoles::where('roleId', $roleId)->get();

    }
}