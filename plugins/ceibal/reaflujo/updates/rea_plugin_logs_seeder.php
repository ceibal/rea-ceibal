<?php namespace Ceibal\ReaFlujo\Updates;

use Ceibal\Rea\Models\Recurso;
use Ceibal\ReaFlujo\Models\ReaPluginLog;
use Ceibal\ReaFlujo\Models\ReaPluginEtapa;
use Seeder;

class ReaPluginLogsSeeder extends Seeder
{
    public function run()
    {
        /*
        Dejo los recursos en una etapa por defecto si no tienen etapas
        */


       self::asignarEtapas();
    }

    private static function asignarEtapas()
    {
        $recursosId = self::obtenerRecursosId();
        $etapaUno = ReaPluginEtapa::where('code', 'etapaUno')->get();
        foreach ($recursosId as $recursoId) {
            $auxRecursoId = ReaPluginLog::obtenerLogsXRecursoId($recursoId);
            /**
             * Si no devuelve nada es que no hay etapas asignadas y por lo tanto le asigno la etapa uno a los recursos;
             */
            if (!$auxRecursoId) {

                $reaPluginLog = ReaPluginLog::firstOrCreate([
                    'etapaId' => $etapaUno[0]->id,
                    'recursoId' => $recursoId,
                    'usuarioId' => 0
                ]);
                $reaPluginLog->created_at = date('Y-m-d H:i:s');
                $reaPluginLog->updated_at = date('Y-m-d H:i:s');
                $reaPluginLog->save();
            }
        }
    }

    private static function obtenerRecursosId()
    {
        $recursos =  Recurso::all();
        $recursosId = [];
        foreach ($recursos as $recurso)
        {
            array_push($recursosId, $recurso->id);

        }

        return $recursosId;
    }
}
