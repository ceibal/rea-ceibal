<?php namespace Ceibal\ReaFlujo\Updates;

use Ceibal\ReaFlujo\Models\ReaPluginEtapa;
use Ceibal\ReaFlujo\Models\ReaPluginLog;
use Ceibal\ReaFlujo\Models\ReaPluginEtapaUserRoles;
use Ceibal\ReaFlujo\Models\ReaPluginEtapaUserGroups;
use Seeder;

class ReaPluginEtapasSeeder extends Seeder
{
    public function run()
    {
        echo "\n ReaPluginEtapasSeeder \n";
        /*
        Cargo las etapas
        */
        $reaPluginEtapa = ReaPluginEtapa::firstOrCreate([
            'nombre' => 'Etapa 1',
            'code' => 'etapaUno',
            'descripcion' => 'Crear contenido',
            'accion' => 'Enviar a correcion de diseño instruccional',
            'estado' => 'Creacion'
        ]);
        $reaPluginEtapa->created_at = date('Y-m-d H:i:s');
        $reaPluginEtapa->updated_at = date('Y-m-d H:i:s');
        $reaPluginEtapa->save();

        $reaPluginEtapa = ReaPluginEtapa::firstOrCreate([
            'nombre' => 'Etapa 2',
            'code' => 'etapaDos',
            'descripcion' => 'Corrector de diseño instruccional',
            'accion' => 'Enviar a correcion de estilo',
            'estado' => 'Correcion diseño instruccional'
        ]);
        $reaPluginEtapa->created_at = date('Y-m-d H:i:s');
        $reaPluginEtapa->updated_at = date('Y-m-d H:i:s');
        $reaPluginEtapa->save();

        $reaPluginEtapa = ReaPluginEtapa::firstOrCreate([
                'nombre' => 'Etapa 3',
                'code' => 'etapaTres',
                'descripcion' => 'Corrector de estilo',
                'accion' => 'Enviar a correcion de diseño gráfico',
                'estado' => 'Correcion de estilo'
        ]);
        $reaPluginEtapa->created_at = date('Y-m-d H:i:s');
        $reaPluginEtapa->updated_at = date('Y-m-d H:i:s');
        $reaPluginEtapa->save();

        $reaPluginEtapa = ReaPluginEtapa::firstOrCreate([
                'nombre' => 'Etapa 4',
                'code' => 'etapaCuatro',
                'descripcion' => 'Corrector de diseño gráfico',
                'accion' => 'Enviar a publicacion',
                'estado' => 'Correcion de diseño gráfico'
        ]);
        $reaPluginEtapa->created_at = date('Y-m-d H:i:s');
        $reaPluginEtapa->updated_at = date('Y-m-d H:i:s');
        $reaPluginEtapa->save();

        $reaPluginEtapa = ReaPluginEtapa::firstOrCreate([
                'nombre' => 'Etapa 5',
                'code' => 'etapaCinco',
                'descripcion' => 'Aprobación de recursos y publicación',
                'estado' => 'Aprobacion de recursos'

        ]);
        $reaPluginEtapa->created_at = date('Y-m-d H:i:s');
        $reaPluginEtapa->updated_at = date('Y-m-d H:i:s');
        $reaPluginEtapa->save();
    }
}
