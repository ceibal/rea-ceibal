<?php namespace Ceibal\ReaFlujo\Updates;

use Backend\Models\UserGroup;
use Ceibal\ReaFlujo\Models\ReaPluginEtapa;
use Ceibal\ReaFlujo\Models\ReaPluginEtapaUserGroups;
use Seeder;

class ReaPluginEtapasUserGroupsSeeder extends Seeder
{
			/*
	Esta funcion se encarga de insertar los groups correspondientes
	*/
	private function insertarGroup($userRoleCode='', $etapasCode=[])
	{
		try
		{
			# obtengo la id del group
			$group = UserGroup::where('code', $userRoleCode)->get();
			# Para cada group le asocio su etapa correspondiete
			foreach ($etapasCode as $etapaCode) 
			{
				$etapa = ReaPluginEtapa::where('code', $etapaCode)->get();
				$etapaUserGroup = ReaPluginEtapaUserGroups::firstOrCreate([
					'groupId'  => $group[0]->attributes['id'],
					'etapaId' => $etapa[0]->attributes['id']
				]);
	        	$etapaUserGroup->created_at = date('Y-m-d H:i:s');
	        	$etapaUserGroup->updated_at = date('Y-m-d H:i:s');
	        	$etapaUserGroup->save();
			}
		}catch(Exception $e)
		{
		}


	}

	public function run()
	{
        echo "\n ReaPluginEtapasUserGroupsSeeder \n";

		$todasLasEtapas = ['etapaUno', 'etapaDos', 'etapaTres', 'etapaCuatro', 'etapaCinco'];

		$administradorGlobal = 'administradorGlobal';
		self::insertarGroup($administradorGlobal, $todasLasEtapas);

		$owner = 'owners';
		self::insertarGroup($owner, $todasLasEtapas);

		$correctorDeEstilo = 'correctorDeEstilo';
		self::insertarGroup($correctorDeEstilo, ['etapaTres']);

		$correctorDeDisenoGrafico = 'correctorDeDisenoGrafico';
		self::insertarGroup($correctorDeDisenoGrafico, ['etapaCuatro']);


		$creadorEditorCorrectorYAprobadorDeRecursos = 'creadorEditorCorrectorYAprobadorDeRecursos';
		self::insertarGroup($correctorDeDisenoGrafico, $todasLasEtapas);

	}
}
