<?php namespace Ceibal\ReaFlujo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CeibalReaRecursoAlterTable extends Migration
{
    public function up()
    {
        echo "\n CreateReaPluginTables \n";
   
        try {
            \DB::statement('SET FOREIGN_KEY_CHECKS=0');
            self::create_tables();
        } catch (Exception $e) {
            \DB::statement('SET FOREIGN_KEY_CHECKS=1');
        }
    }

    private function create_tables()
    {

        Schema::table('ceibal_rea_recursos', function($table){
            $table->integer('etapaUno')->default(0);
            $table->integer('etapaDos')->default(0);
            $table->integer('etapaTres')->default(0);
            $table->integer('etapaCuatro')->default(0);
            $table->integer('etapaCinco')->default(0);
            # $table->string('estado')->default(0);
            # $table->string('usuario')->default(0);
        });
    }

    public function down()
    {
        Schema::table('ceibal_rea_recursos', function($table){
            $table->dropColumn('etapaUno');
            $table->dropColumn('etapaDos');
            $table->dropColumn('etapaTres');
            $table->dropColumn('etapaCuatro');
            $table->dropColumn('etapaCinco');
        });
    }
}  