<?php namespace Ceibal\ReaFlujo\Updates;

use Backend\Models\UserGroup;
use Seeder;

class SeederBackendUserGroupsSeeder extends Seeder
{
	public function run(){
		$backend_user_groups = UserGroup::firstOrCreate([
			'name' => 'Administrador global',
			'code' => 'administradorGlobal',
			'description' => 'Puede hacer todo, todas las etapas'
		]);

		$backend_user_groups = UserGroup::firstOrCreate([
			'name' => 'Corrector de Estilo',
			'code' => 'correctorDeEstilo',
			'description' => 'Corrige el estilo, etapa 3'
		]);

		$backend_user_groups = UserGroup::firstOrCreate([
			'name' => 'Corrector de diseño gráfico',
			'code' => 'correctorDeDisenoGrafico',
			'description' => 'Corrige el diseño gráfico, etapa 4'
		]);

		$backend_user_groups = UserGroup::firstOrCreate([
			'name' => 'Creador, editor, corrector y aprobador de recursos',
			'code' => 'creadorEditorCorrectorYAprobadorDeRecursos',
			'description' => 'Crea, Edita, Borra, Publica, despublica. Puede marcar recursos como \“destacado\”, todas las etapas'
		]);

		$backend_user_groups = UserGroup::firstOrCreate([
			'name' => 'Contenidista externo',
			'code' => 'contenidistaExterno',
			'description' => 'Crea, Edita, etapa 0'
		]);

		$backend_user_groups = UserGroup::firstOrCreate([
			'name' => 'Usuario final',
			'code' => 'usuarioFinal',
			'description' => 'Crea, etapa 0'
		]);
	}
}
