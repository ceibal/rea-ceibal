<?php namespace Ceibal\ReaFlujo\Updates;

use Backend\Models\UserRole;
use Seeder;

class SeederBackendUserRoles extends Seeder
{
	public function run()
	{

		//$full_permisos = ["ceibal.rea.reorder_categorias":"1","ceibal.rea.access_categorias":"1","ceibal.rea.access":"1","ceibal.rea.licencias":"1"];
		$full_permisos = ["ceibal.rea.reorder_categorias","ceibal.rea.access_categorias","ceibal.rea.access","ceibal.rea.licencias"];
		//$solo_rea_permisos = '{"ceibal.rea.reorder_categorias":"0","ceibal.rea.access_categorias":"0","ceibal.rea.access":"1","ceibal.rea.licencias":"0"}';
		$solo_rea_permisos = ["ceibal.rea.access"];

		$backend_user_roles = UserRole::firstOrCreate([
			'name' => 'Administrador global',
			'permissions'=> $full_permisos,
			'code' => 'administradorGlobal',
			'description' => 'Puede hacer todo, todas las etapas',
		]);

		$backend_user_roles = UserRole::firstOrCreate([
			'name' => 'Corrector diseno Instruccional',
			'permissions'=> $solo_rea_permisos,
			'code' => 'correctorInstruccional',
			'description' => 'Corrige diseno instruccional, etapa 2',
		]);

		$backend_user_roles = UserRole::firstOrCreate([
			'name' => 'Corrector de Estilo',
			'permissions'=> $solo_rea_permisos,
			'code' => 'correctorDeEstilo',
			'description' => 'Corrige el estilo, etapa 3',
		]);

		$backend_user_roles = UserRole::firstOrCreate([
			'name' => 'Corrector de diseño gráfico',
			'code' => 'correctorDeDisenoGrafico',
			'description' => 'Corrige el diseño gráfico, etapa 4',

		]);

		$backend_user_roles = UserRole::firstOrCreate([
			'name' => 'Creador, editor, corrector y aprobador de recursos',
			'permissions'=> $solo_rea_permisos,
			'code' => 'creadorEditorCorrectorYAprobadorDeRecursos',
			'description' => 'Crea, Edita, Borra, Publica, despublica. Puede marcar recursos como \“destacado\”, todas las etapas',
		]);

		$backend_user_roles = UserRole::firstOrCreate([
			'name' => 'Contenidista externo',
			'permissions'=> $solo_rea_permisos,
			'code' => 'contenidistaExterno',
			'description' => 'Crea, Edita, etapa 0',
		]);

		$backend_user_roles = UserRole::firstOrCreate([
			'name' => 'Usuario final',
			'permissions'=> $solo_rea_permisos,
			'code' => 'usuarioFinal',
			'description' => 'Crea, etapa 0',
		]);


	}

}
