<?php
    use Ceibal\ReaFlujo\Models\ReaPluginLog;
    use Backend\Facades\BackendAuth;

    Route::get('/backend/ceibal/reaflujo/gethistorial', function()
    {



        //  if(BackendAuth::check())
        //  {

            $id = $_REQUEST["id"];


            $result = DB::select("SELECT *, rea_plugin_log.created_at as created FROM rea_plugin_log
            JOIN backend_users ON rea_plugin_log.usuarioId = backend_users.id
            JOIN rea_plugin_etapa ON rea_plugin_log.etapaId = rea_plugin_etapa.id
            WHERE recursoId = ?
            AND usuarioId > 0
            ORDER BY rea_plugin_log.id DESC ", [$id]);

        //  }
        return Response::json($result);
    });
?>
